using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EasyRoads3Dv3;



public class EasyRoadsWaypointExport : MonoBehaviour
{

    public float waypointDistance = 5f;
    public float angleThreshold = 1f;

    public List<ERRoad> roads;
    public Dictionary<string, bool> roadSelected;


}