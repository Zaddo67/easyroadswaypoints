using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using EasyRoads3Dv3;


[CustomEditor(typeof(EasyRoadsWaypointExport))]
public class EasyRoadsWaypointExportEditor : Editor
{

    enum positionCategory { LeftLane, RightLane, Center, Left, Right }

    private static GUIContent genLeftLane = new GUIContent("LeftLane", "Generate Left Lane Waypoints");
    private static GUIContent genRightLane = new GUIContent("RightLane", "Generate Right Lane Waypoints");
    private static GUIContent genCenter = new GUIContent("Center", "Generate Center Waypoints");
    private static GUIContent genLeft = new GUIContent("Left", "Generate Left Waypoints");
    private static GUIContent genRight = new GUIContent("Right", "Generate Right Waypoints");
    private static GUIContent genClear = new GUIContent("Clear", "Clear Waypoints");

    EasyRoadsWaypointExport p;
    SerializedProperty waypointDistance;
    SerializedProperty angleThreshold;


    GUIStyle boxStyle;
    private Color softGreen = new Color(.67f, .89f, .67f, 1f);


    void OnEnable()
    {
        p = (EasyRoadsWaypointExport)target;
        waypointDistance = serializedObject.FindProperty("waypointDistance");
        angleThreshold = serializedObject.FindProperty("angleThreshold");

        LoadRoads();

        if (Application.isPlaying) return;

    }

    private void LoadRoads()
    {
        if (p == null) return;

        ERRoadNetwork roadNetwork = new ERRoadNetwork();

        var roads = roadNetwork.GetRoads();

        p.roads = new List<ERRoad>();
        p.roadSelected = new Dictionary<string, bool>();

        foreach (var r in roads)
        {
            p.roads.Add(r);
            p.roadSelected.Add(r.GetName(), false);
        }

    }

    public override void OnInspectorGUI()
    {
        p = (EasyRoadsWaypointExport)target;
        if (p == null) return;

        serializedObject.Update();


        // Check if any control changed between here and EndChangeCheck
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(waypointDistance, true);
        EditorGUILayout.PropertyField(angleThreshold, true);

        boxStyle = GUI.skin.box;

        EditorGUILayout.BeginVertical(boxStyle);
        {
            EditorShowRoadList();
        }
        EditorGUILayout.EndVertical();


        EditorGUILayout.BeginVertical(boxStyle);
        {
            EditorShowButtons();
        }
        EditorGUILayout.EndVertical();

        // If any control changed, then apply changes
        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
        }

    }

    private void EditorShowButtons()
    {
        EditorGUILayout.LabelField("Generate Waypoints:", EditorStyles.boldLabel);

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button(genLeftLane, EditorStyles.miniButton, GUILayout.MaxWidth(50f)))
        {
            GenerateWaypoints(positionCategory.LeftLane);
        }

        if (GUILayout.Button(genRightLane, EditorStyles.miniButton, GUILayout.MaxWidth(50f)))
        {
            GenerateWaypoints(positionCategory.RightLane);

        }

        if (GUILayout.Button(genCenter, EditorStyles.miniButton, GUILayout.MaxWidth(50f)))
        {
            GenerateWaypoints(positionCategory.Center);
        }

        EditorGUILayout.EndHorizontal();


        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button(genLeft, EditorStyles.miniButton, GUILayout.MaxWidth(50f)))
        {
            GenerateWaypoints(positionCategory.Left);
        }

        if (GUILayout.Button(genRight, EditorStyles.miniButton, GUILayout.MaxWidth(50f)))
        {
            GenerateWaypoints(positionCategory.Right);

        }

        if (GUILayout.Button(genClear, EditorStyles.miniButton, GUILayout.MaxWidth(50f)))
        {
            ClearAllWaypoints();
        }

        EditorGUILayout.EndHorizontal();

    }


    private void EditorShowRoadList()
    {
        if (p.roads == null) return;
        if (p.roads.Count <= 0) return;

        try
        {

            foreach (var r in p.roads)
            {
                EditorGUIShowRoadString(r);
            }

        }
        catch (Exception ex)
        {
            Debug.Log("Error displaying roads: " + ex.Message);
        }

    }

    /// <summary>
    /// Dispalay Road 
    /// </summary>
    /// <param name="sceneId"></param>
    private void EditorGUIShowRoadString(ERRoad road)
    {
        EditorGUILayout.BeginHorizontal();

        var currentVal = p.roadSelected[road.GetName()];
        var newVal = EditorGUILayout.Toggle(currentVal);
        if (currentVal != newVal)
        {
            p.roadSelected[road.GetName()] = newVal;
        }


        EditorGUILayout.LabelField(road.GetName(), EditorStyles.label);


        EditorGUILayout.EndHorizontal();
    }

    private void GenerateWaypoints(positionCategory pos)
    {


        ERRoadNetwork roadNetwork = new ERRoadNetwork();

        foreach (var r in p.roadSelected)
        {
            if (r.Value)
            {
                var road = roadNetwork.GetRoadByName(r.Key);
                if (road != null)
                {
                    GameObject waypoints = GetWaypointObject(r.Key, pos);

                    if (waypoints != null)
                    {
                        ClearWaypointObjects(waypoints);
                        CalcWaypoints(waypoints.transform, road, pos);
                    }
                }
                else
                {
                    Debug.Log(String.Format("Error retrieving road {0}", r.Key));
                }

            }
        }

    }

    private GameObject GetWaypointObject(String roadName, positionCategory pos)
    {
        GameObject waypointObject;

        var roadObjects = p.transform.Find("Road Objects");
        if (roadObjects != null)
        {
            var findRoad = roadObjects.Find(roadName);
            if (findRoad != null)
            {

                var findObject = findRoad.Find(pos.ToString());

                if (findObject == null)
                {
                    waypointObject = new GameObject();
                    waypointObject.name = pos.ToString();
                    waypointObject.transform.position = p.transform.position;
                    waypointObject.transform.rotation = p.transform.rotation;
                    waypointObject.transform.parent = findRoad;
                }
                else waypointObject = findObject.gameObject;

                return waypointObject;
            }
            else Debug.Log(String.Format("Could not find road transform {0}", roadName));
        }
        else Debug.Log("Could not find Road Objects transform");

        return null;
    }

    /// <summary>
    /// Calculate waypoints along the road
    /// </summary>
    /// <param name="roadName">Name of the road.</param>
    private void CalcWaypoints(Transform parent, ERRoad road, positionCategory pos)
    {

        Quaternion rot = new Quaternion();

        Quaternion splineRot = new Quaternion();
        Quaternion lastRot = new Quaternion();

        float distance = p.waypointDistance;
        float distanceFromLastWaypoint = 0;
        float distanceBetweenSplines = 0;
        float intraSplineDistance = 0;
        float percentageBetweenSplines = 0;
        float deltaAngle = 0;
        int waypointCounter = 0;
        int waypointsSkipped = 0;


        Vector3[] splinePoints = GetSplinePoints(road, pos);

        for (int i = 0; i < splinePoints.Length; i++)
        {

            if (i > 0)
            {
                distanceBetweenSplines = Vector3.Distance(splinePoints[i - 1], splinePoints[i]);
                Vector3 relativePos = splinePoints[i] - splinePoints[i - 1];
                splineRot = Quaternion.LookRotation(relativePos, Vector3.up);
            }


            if (i == 0)  // Beginning
            {
                Vector3 relativePos = splinePoints[i + 1] - splinePoints[i];
                rot = Quaternion.LookRotation(relativePos, Vector3.up);
                CreateWaypoint(parent, splinePoints[i], rot, waypointCounter.ToString());
                waypointCounter++;
            }
            else if (i == (splinePoints.Length - 1))   // End
            {
                Vector3 relativePos = splinePoints[i] - splinePoints[i - 1];
                rot = Quaternion.LookRotation(relativePos, Vector3.up);
                CreateWaypoint(parent, splinePoints[i], rot, waypointCounter.ToString());
                waypointCounter++;
            }
            else if ((distanceFromLastWaypoint + distanceBetweenSplines) > distance)  // Between Beginning and En
            {

                Vector3 relativePos = splinePoints[i + 1] - splinePoints[i];
                rot = Quaternion.LookRotation(relativePos, Vector3.up);

                intraSplineDistance = distanceFromLastWaypoint + distanceBetweenSplines - distance;
                percentageBetweenSplines = intraSplineDistance / distanceBetweenSplines;

                CreateWaypoint(parent, Vector3.Lerp(splinePoints[i - 1], splinePoints[i], percentageBetweenSplines), rot, waypointCounter.ToString());

                distanceFromLastWaypoint = distanceBetweenSplines - intraSplineDistance;
                deltaAngle = 0f;
                waypointsSkipped = 0;

                waypointCounter++;

            }
            else if (deltaAngle > p.angleThreshold)  // If road bends exceed angle threshold then inject extra waypoints
            {
                Vector3 relativePos = splinePoints[i + 1] - splinePoints[i];
                rot = Quaternion.LookRotation(relativePos, Vector3.up);

                CreateWaypoint(parent, splinePoints[i], rot, waypointCounter.ToString());

                distanceFromLastWaypoint = 0f;
                deltaAngle = 0f;
                waypointsSkipped = 0;

                waypointCounter++;
            }
            else
            {
                distanceFromLastWaypoint += distanceBetweenSplines;
                deltaAngle += Mathf.Abs(Quaternion.Angle(lastRot, rot));
                waypointsSkipped++;

            }

            lastRot = splineRot;

        }
    }

    private Vector3[] GetSplinePoints(ERRoad road, positionCategory pos)
    {

        if (pos == positionCategory.Center)
        {
            return road.GetSplinePointsCenter();
        }

        if (pos == positionCategory.Left)
        {
            return road.GetSplinePointsLeftSide();
        }

        if (pos == positionCategory.Right)
        {
            return road.GetSplinePointsRightSide();
        }

        if (pos == positionCategory.LeftLane)
        {
            var center = road.GetSplinePointsCenter();
            var left = road.GetSplinePointsLeftSide();
            List<Vector3> leftLane = new List<Vector3>();

            if (left.Length == center.Length)
            {
                for (int i = 0; i < left.Length; i++)
                {
                    leftLane.Add(Vector3.Lerp(left[i], center[i], 0.5f));
                }
            }
            return leftLane.ToArray();
        }

        if (pos == positionCategory.RightLane)
        {
            var center = road.GetSplinePointsCenter();
            var right = road.GetSplinePointsRightSide();
            List<Vector3> leftLane = new List<Vector3>();

            if (right.Length == center.Length)
            {
                for (int i = 0; i < right.Length; i++)
                {
                    leftLane.Add(Vector3.Lerp(right[i], center[i], 0.5f));
                }
            }
            return leftLane.ToArray();
        }

        return new Vector3[0];

    }

    private void CreateWaypoint(Transform parent, Vector3 pos, Quaternion rot, String name)
    {

        GameObject waypointObject = new GameObject();
        waypointObject.name = name;
        waypointObject.transform.position = pos;
        waypointObject.transform.rotation = rot;
        waypointObject.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        waypointObject.transform.parent = parent;
    }

    private void ClearAllWaypoints()
    {


        ERRoadNetwork roadNetwork = new ERRoadNetwork();

        foreach (var r in p.roadSelected)
        {
            if (r.Value)
            {
                ClearPositionWaypoints(r.Key, positionCategory.Center);
                ClearPositionWaypoints(r.Key, positionCategory.Left);
                ClearPositionWaypoints(r.Key, positionCategory.LeftLane);
                ClearPositionWaypoints(r.Key, positionCategory.Right);
                ClearPositionWaypoints(r.Key, positionCategory.RightLane);
            }
        }

    }

    private void ClearPositionWaypoints(String roadName, positionCategory pos)
    {

        GameObject waypoints = GetWaypointObject(roadName, pos);

        if (waypoints != null)
        {
            ClearWaypointObjects(waypoints);
            DestroyImmediate(waypoints);
        }

    }

    private void ClearWaypointObjects(GameObject waypoints)
    {
        while (waypoints.transform.childCount > 0)
        {
            DestroyImmediate(waypoints.transform.GetChild(0).gameObject);
        }
    }

}

