# EasyRoadsWaypoints

Place the following file into a folder named "Editor"
EasyRoadsWaypointExportEditor.cs

Add the EasyRoadsWaypoints component to your RoadNetwork GameObject

Properties:

WaypointDistance:  Distance Between Waypoints
AngleThreshold: 	Maximum angle of road bend, that will trigger an early waypoint

To run:
Select radio button for the road you want to generate waypoints for. Then click on the correct button for where you want to position the waypoints. An object with the name of the button you clicked will be added as a child to the Road object.


